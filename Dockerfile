# Image link https://hub.docker.com/layers/python/library/python/3.7.13-alpine/images/sha256-116658f8dec51a13dd4631b98d983e656003e2badbb2fb7236a5510b49bbdcdb?context=explore
# SO -> ALPINE    TAG/VERSION -> 3.7.13-alpine
# SHA256 ->  758afaffeca55f84200cc2344373c2569768c0b07f3e79bdfd13a9c64adfc02f
FROM python@sha256:758afaffeca55f84200cc2344373c2569768c0b07f3e79bdfd13a9c64adfc02f

# Define workdir
WORKDIR /home/lambdas-deploy

# Copy python script
COPY src/lambdas-deploy.py /usr/local/bin
# Update repository and install packages to download and install pkgdiff
RUN chmod +x /usr/local/bin/lambdas-deploy.py 

# Copy requirements
COPY src/requirements.txt /tmp

# Update repository and install packages to download and install pkgdiff
RUN pip3 install -r /tmp/requirements.txt

CMD ["lambdas-deploy.py", "--help" ]
