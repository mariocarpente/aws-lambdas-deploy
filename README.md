# AWS Lambdas Deploy
This repository stores a python script that publishes a new version of an AWS lambda and applies a canary deployment using a predefined alias. The code used is a modification of the sample code in this [repository](https://github.com/aws-samples/aws-lambda-deploy).

The script has been packaged in a docker image so that it can be used as an image in a CI/CD pipeline. The [image](https://hub.docker.com/repository/docker/marcarvar/aws-lambdas-deploy) is uploaded to DockerHub.

## ENV VARS
This script need the following enviroment variables:
* **AWS_ACCESS_KEY_ID**
* **AWS_SECRET_ACCESS_KEY**
* **AWS_SESSION_TOKEN** (optional) 
* **AWS_REGION** or **AWS_DEFAULT_REGION** 


## Execution options
The available options and arguments are shown below:

```sh
$ python3 src/lambdas-deploy.py --help
Usage: lambdas-deploy.py [OPTIONS] ALIAS_NAME FUNCTION_NAME PATH

  Publish new version of lambda with canary deploy

Arguments:
  ALIAS_NAME     Name of the alias used to invoke the Lambda function.
                 [required]
  FUNCTION_NAME  Name of the Lambda function to deploy.  [required]
  PATH           Path to the file or folder that make up the code for the
                 lambda.  [required]

Options:
  --steps INTEGER                 Number of times the new version weight is
                                  increased.  [default: 10]
  --interval INTEGER              The amount of time (in seconds) to wait
                                  between weight updates.  [default: 15]
  --health-check / --no-health-check
                                  Check the state of lambda and if an error
                                  occurs, rollback.  [default: health-check]
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.
  --help                          Show this message and exit.
```