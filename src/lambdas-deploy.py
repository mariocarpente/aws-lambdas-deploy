#!/usr/local/bin/python3
import os, zipfile
import shutil
import time
import boto3, botocore
import typer
from datetime import datetime, date, timedelta
from pathlib import Path


lambda_client = None
# Env vars
aws_access_key_id = None
aws_secret_access_key = None
aws_session_token = None
region_name = None

TEMP_DIR = "./temp/"

def create_zip_file(path):
    path = str(path)
    zip_name = TEMP_DIR
    if os.path.isfile(path):
        zip_name += os.path.splitext(path)[0].split("/")[-1]+".zip"
        with zipfile.ZipFile(zip_name, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zf:
            zf.write(path, path.split("/")[-1])        
    if os.path.isdir(path):
        zip_name += path.split("/")[-1]
        shutil.make_archive(zip_name, 'zip', path)
        zip_name += ".zip"
    return zip_name


def update_lambda_code(function_name, path):
    client = get_lambda_client()
    zip_path = create_zip_file(path)
    try:
        with open(zip_path, "rb") as f:
            response = client.update_function_code(
                FunctionName= function_name,
                ZipFile=f.read(),
                Publish=True
            )
            return response["Version"]
    except botocore.exceptions.ClientError as error:        
        if os.path.exists(TEMP_DIR):
            shutil.rmtree(TEMP_DIR)
        if error.response['Error']['Code'] == 'ResourceNotFoundException':
            raise Exception("Lambda fuction with name "+function_name+" not exist in region "+ client.meta.region_name)
        else:
            raise error



def update_weight(func_name, alias_name, version, next_weight):
    print("Updating weight of alias {1}:{2} for version {0} to {3}".format(version, func_name, alias_name, next_weight), end='\n')
    client = get_lambda_client()

    weights = {
        version : next_weight
    }
    routing_config = {
        'AdditionalVersionWeights' : weights
    }

    client.update_alias(FunctionName=func_name, Name=alias_name, RoutingConfig=routing_config)
    return

def sleep(sleep_time):
    time.sleep(sleep_time)

def validate_input(steps, interval, path):
    if steps < 1:
        raise Exception("'steps' must be greater than 0")
    if interval < 1:
        raise Exception("'interval' must be greater than 0")
    if not os.path.exists(path):
        raise Exception("Path "+path+" not exist")

def do_health_check(func_name, alias_name, version):
    # implement custom health checks here (i.e. invoke, cloudwatch alarms, etc)
    return check_errors_in_cloudwatch(func_name, alias_name, version)

# Return False if any error metrics were emitted in the last minute for the function/alias/new version combination
def check_errors_in_cloudwatch(func_name, alias_name, new_version):
    client = boto3.client(
            'cloudwatch',
            aws_access_key_id= aws_access_key_id,
            aws_secret_access_key= aws_secret_access_key,
            aws_session_token= aws_session_token,
            region_name= region_name)

    func_plus_alias = func_name + ":" + alias_name
    now = datetime.utcnow()
    start_time = now - timedelta(minutes=1)

    response = client.get_metric_statistics(
        Namespace='AWS/Lambda',
        MetricName='Errors',
        Dimensions=[
            {
                'Name': 'FunctionName',
                'Value': func_name
            },
            {
                'Name': 'Resource',
                'Value': func_plus_alias
            },
            {
                'Name': 'ExecutedVersion',
                'Value': new_version
            }
        ],
        StartTime=start_time,
        EndTime=now,
        Period=60,
        Statistics=['Sum']
    )
    datapoints = response['Datapoints']
    for datapoint in datapoints:
        if datapoint['Sum'] > 0:
            print("Failing health check because error metrics were found for new version: {0}".format(datapoints), end='\n')
            return False

    return True

def rollback(func_name, alias_name, version):
    print("Health check failed. Rolling back to original version", end='\n')
    client = get_lambda_client()
    routing_config = {
        'AdditionalVersionWeights' : {}
    }
    client.update_alias(FunctionName=func_name, Name=alias_name, RoutingConfig=routing_config)
    client.delete_function(FunctionName=func_name, Qualifier=version)

    print("Alias was successfully rolled back to original version", end='\n')
    return

# Set the new version as the primary version and reset the AdditionalVersionWeights
def finalize(func_name, alias_name, version):
    client = get_lambda_client()
    routing_config = {
        'AdditionalVersionWeights' : {}
    }
    res = client.update_alias(FunctionName=func_name, FunctionVersion=version, Name=alias_name, RoutingConfig=routing_config)
    return res

def generate_weights(type, steps):
    if type == "linear":
        values = linear(steps)
    # implement other functions here
    else:
        raise Exception("Invalid function type: " + type)
    return values

def get_num_points(time, interval):
    return time / interval

def linear(num_points):
    delta = 1.0 / num_points
    prev = 0
    values = []
    for i in range(0, num_points):
        val = prev + delta
        values.append(round(val, 2))
        prev = val
    return values

def get_lambda_client():
    global lambda_client
    if lambda_client is None:
        lambda_client = boto3.client(
                        'lambda',
                        aws_access_key_id= aws_access_key_id,
                        aws_secret_access_key= aws_secret_access_key,
                        aws_session_token= aws_session_token,
                        region_name= region_name)
    return lambda_client

def init_aws_env_vars():
    global aws_access_key_id, aws_secret_access_key, aws_session_token, region_name
    # Get and check AWS_ACCESS_KEY_ID enviroment variable
    aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
    if aws_access_key_id is None:
        raise Exception("Enviroment variable AWS_ACCESS_KEY_ID not found")
    # Get and check AWS_SECRET_ACCESS_KEY enviroment variable
    aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    if aws_secret_access_key is None:
        raise Exception("Enviroment variable AWS_SECRET_ACCESS_KEY not found")
    # Get AWS_SESSION_TOKEN enviroment variable
    aws_session_token = os.environ.get('AWS_SESSION_TOKEN')
    # Get AWS_REGION and AWS_DEFAULT_REGION enviroment variable
    region_name = os.environ.get('AWS_REGION')
    aws_default_region = os.environ.get('AWS_DEFAULT_REGION')
    if region_name is None and aws_default_region is None:
        raise Exception("Enviroment variable AWS_REGION or AWS_DEFAULT_REGION not found")
    # If AWS_REGION is not define use AWS_DEFAULT_REGION
    if region_name is None:
        region_name = aws_default_region

def main(
    alias_name: str = typer.Argument(..., help="Name of the alias used to invoke the Lambda function."),
    function_name: str = typer.Argument(..., help="Name of the Lambda function to deploy."),
    path: Path = typer.Argument(..., help="Path to the file or folder that make up the code for the lambda."),
    steps: int = typer.Option(10, help="Number of times the new version weight is increased."),
    interval: int = typer.Option(15, help="The amount of time (in seconds) to wait between weight updates."),
    health_check: bool = typer.Option(True, help="Check the state of lambda and if an error occurs, rollback."),
):
    """
    Publish new version of lambda with canary deploy
    """ 
    # Validate values
    validate_input(steps, interval, path)
    # Get credentials env variables
    init_aws_env_vars() 
    # Calculate weights
    weight_function = "linear"
    weights = generate_weights(weight_function, steps)
    start_time = time.time()
    # Update lambda code and publish new version
    print("Publishing new version of {0} lambda".format(function_name), end='\n')
    if not os.path.exists(TEMP_DIR):
        os.mkdir(TEMP_DIR)
    version = update_lambda_code(function_name, path)
    if os.path.exists(TEMP_DIR):
        shutil.rmtree(TEMP_DIR)
    print("New version publish with number {0}".format(version), end='\n')

    print("Calculated alias weight progression: {0}".format(weights), end='\n')

    for weight in weights:
        update_weight(function_name, alias_name, version, weight)
        sleep(interval)

        if health_check:
            success = do_health_check(function_name, alias_name, version)
            if not success:
                rollback(function_name, alias_name, version)
                raise Exception("Health check failed, exiting")

    res = finalize(function_name, alias_name, version)
    end_time = time.time()

    print("Alias {0}:{1} is now routing 100% of traffic to version {2}".format(function_name, alias_name, version), end='\n')
    print("Finished after {0}s".format(round(end_time - start_time, 2)), end='\n')

    return res


if __name__ == "__main__":
    typer.run(main)
